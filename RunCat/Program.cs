﻿// Copyright 2020 Takuto Nakamura
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

using RunCat.Properties;
using Microsoft.Win32;
using System;
using System.IO;
using System.Xml.Serialization;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;
using System.Windows.Forms;

namespace RunCat
{

    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new RunCatApplicationContext());
        }
    }

    public class Settings
    {
        public bool isSystemsColorCat = true;
        public bool isSystemsColorBg = true;
        public int colorBg = Color.Transparent.ToArgb();
        public int colorCat = Color.FromArgb(255, 255, 255).ToArgb();

        [XmlIgnore]
        public Color ColorBg
        {
            get
            {
                return Color.FromArgb(colorBg);
            }
            set
            {
                colorBg = value.ToArgb();
            }
        }
        [XmlIgnore]
        public Color ColorCat
        {
            get
            {
                return Color.FromArgb(colorCat);
            }
            set
            {
                colorCat = value.ToArgb();
            }
        }

        public static Settings LoadSettings(string fname)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Settings));
            Settings ret;
            try
            {
                using (FileStream fs = new FileStream(fname, FileMode.Open, FileAccess.Read))
                {
                    ret = serializer.Deserialize(fs) as Settings;
                }
            }
            catch (Exception)
            {
                ret = null;
            }

            if (ret == null)
            {
                ret = new Settings();
            }

            return ret;
        }
        public void SaveSettings(string fname)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Settings));
            using (FileStream fs = new FileStream(fname, FileMode.Create, FileAccess.Write))
            {
                serializer.Serialize(fs, this);
            }
        }
    }

    public class RunCatApplicationContext: ApplicationContext
    {
        private string settingsFilename = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "setting.xml");
        private PerformanceCounter cpuUsage;
        private NotifyIcon notifyIcon;
        private int current = 0;
        private Icon[] icons;
        private Bitmap[] imgIconBufs;
        private Timer animateTimer = new Timer();
        private Timer cpuTimer = new Timer();
        private Settings settings;


        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        extern static bool DestroyIcon(IntPtr handle);

        private static bool IsLight(Color color)
        {
            const int THRESHOLD = 255 * 3 / 2;
            int brightness = (color.R + color.G + color.B) * color.A / 0xFF;
            return (brightness > THRESHOLD);
        }

        private static void SafeCall(Action a)
        {
            try
            {
                a?.Invoke();
            }
            catch (Exception)
            {

            }
        }

        public RunCatApplicationContext()
        {
            SystemEvents.UserPreferenceChanged += new UserPreferenceChangedEventHandler(UserPreferenceChanged);

            cpuUsage = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            _ = cpuUsage.NextValue(); // discards first return value

            settings = Settings.LoadSettings(settingsFilename);

            InitNotifyMenu();
            InitIconBuffer();
            InitAnimeTimer();
            InitCpuTimer();

            CpuTick();
            cpuTimer.Start();
            current = 0;

            UpdateIcons();
        }



        private bool IsSystemUsesLightTheme()
        {
            const string SUB_KEY_NAME = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize";
            using (RegistryKey rKey = Registry.CurrentUser.OpenSubKey(SUB_KEY_NAME))
            {
                object v = rKey?.GetValue(@"SystemUsesLightTheme");
                int? n = v as int?;
                return bool.Equals(n?.Equals(1), true);
            }
        }


        private Bitmap[] GetCatImages()
        {
            return new Bitmap[]
            {
                Resources.cat16_0,
                Resources.cat16_1,
                Resources.cat16_2,
                Resources.cat16_3,
                Resources.cat16_4,
            };
        }

        private void SetIconColor(Color catColor, Color bgColor)
        {
            Bitmap[] images = GetCatImages();
            ImageAttributes attr = new ImageAttributes();

            //　remap color table
            {
                ColorMap[] colorMap = new ColorMap[256];
                for (int j = 0; j < colorMap.Length; j++)
                {
                    colorMap[j] = new ColorMap()
                    {
                        OldColor = Color.FromArgb(j, 255, 255, 255),
                        NewColor = Color.FromArgb(j * catColor.A / 255, catColor.R, catColor.G, catColor.B),
                    };
                }
                attr.SetRemapTable(colorMap);
            }

            //  recreate icons
            for (int i  = 0; i < imgIconBufs.Length; i++)
            {
                Bitmap imgBuf = imgIconBufs[i];
                Bitmap imgCat = images[i];
                Icon ico = icons[i];
                // bg
                {
                    Graphics g = Graphics.FromImage(imgBuf);
                    g.Clear(bgColor);
                    g.DrawImage(imgCat, new Rectangle(new Point(0, 0), imgCat.Size), 0, 0, imgCat.Width, imgCat.Height, GraphicsUnit.Pixel, attr);
                }
                if(icons[i] != null)
                {
                    DestroyIcon(icons[i].Handle);
                    icons[i] = null;
                }
                icons[i] = Icon.FromHandle(imgBuf.GetHicon());
            }
        }

        private void UpdateIcons()
        {
            // init for dark  theme.
            Color colorCat = Color.FromArgb(255, 255, 255);
            Color colorBg = Color.Transparent;

            if (settings.isSystemsColorCat | settings.isSystemsColorBg)
            {
                if (IsSystemUsesLightTheme())
                {
                    // set for light  theme.
                    colorCat = Color.FromArgb(0, 0, 0);
                    colorBg = Color.Transparent;
                }
            }

            if (!settings.isSystemsColorCat)
            {
                // self color.
                colorCat = settings.ColorCat;
            }
            if (!settings.isSystemsColorBg)
            {
                // self color.
                colorBg = settings.ColorBg;
            }
            SetIconColor(
                colorCat,
                colorBg
            );
        }

        private void InitNotifyMenu()
        {
            notifyIcon = new NotifyIcon()
            {
                Icon = Resources.light_cat0,
                ContextMenu = new ContextMenu(new MenuItem[]
                {
                    new MenuItem("Cat - color from system", delegate (object sender, EventArgs e) {
                        settings.isSystemsColorCat = true;
                        UpdateIcons();
                    }),
                    new MenuItem("Cat - select color ...", delegate (object sender, EventArgs e) {
                        ColorDialog colorDialog = new ColorDialog();
                        colorDialog.AllowFullOpen = true;
                        if (colorDialog.ShowDialog() == DialogResult.OK)
                        {
                            settings.ColorCat = colorDialog.Color;
                            settings.isSystemsColorCat = false;
                            UpdateIcons();
                        }
                    }),
                    new MenuItem("Cat - set transparent", delegate (object sender, EventArgs e) {
                            settings.ColorCat = Color.Transparent;
                            settings.isSystemsColorCat = false;
                            UpdateIcons();
                    }),
                    new MenuItem("-"),
                    new MenuItem("BG - color for system", delegate (object sender, EventArgs e) {
                        settings.isSystemsColorBg = true;
                        UpdateIcons();
                    }),
                    new MenuItem("BG - select color ...", delegate (object sender, EventArgs e) {
                        ColorDialog colorDialog = new ColorDialog();
                        colorDialog.AllowFullOpen = true;
                        if (colorDialog.ShowDialog() == DialogResult.OK)
                        {
                            settings.ColorBg = colorDialog.Color;
                            settings.isSystemsColorBg = false;
                            UpdateIcons();
                        }
                    }),
                    new MenuItem("BG - set transparent", delegate (object sender, EventArgs e) {
                            settings.ColorBg = Color.Transparent;
                            settings.isSystemsColorBg = false;
                            UpdateIcons();
                    }),
                    new MenuItem("-"),
                    new MenuItem("Exit", MenuExit),
                }),
                Text = "0.0%",
                Visible = true
            };
        }

        private void InitIconBuffer()
        {
            Image[] images = GetCatImages();

            imgIconBufs = new Bitmap[images.Length];
            for (int i = 0; i < imgIconBufs.Length; i++)
            {
                imgIconBufs[i] = new Bitmap(16, 16, PixelFormat.Format32bppArgb);
            }

            icons = new Icon[images.Length];
        }

        private void InitAnimeTimer()
        {
            animateTimer.Interval = 200;
            animateTimer.Tick += new EventHandler(AnimationTickEvent);
        }

        private void InitCpuTimer()
        {
            cpuTimer.Interval = 3000;
            cpuTimer.Tick += new EventHandler(CpuTickEvent);
        }

        private void AnimationTick()
        {
            if (icons?.Length > 0)
            {
                current = (current + 1) % icons.Length;
                notifyIcon.Icon = icons[current];
            }
        }
        private void CpuTick()
        {
            float s = cpuUsage.NextValue();
            notifyIcon.Text = $"{s:f1}%";
            s = 200.0f / (float)Math.Max(1.0f, Math.Min(20.0f, s / 5.0f));
            animateTimer.Stop();
            animateTimer.Interval = (int)s;
            animateTimer.Start();
        }

        private void AnimationTickEvent(object sender, EventArgs e)
        {
            AnimationTick();
        }

        private void CpuTickEvent(object sender, EventArgs e)
        {
            CpuTick();
        }

        private void UserPreferenceChanged(object sender, UserPreferenceChangedEventArgs e)
        {
            if (e.Category == UserPreferenceCategory.General)
            {
                UpdateIcons();
            }
        }

        private void MenuExit(object sender, EventArgs e)
        {
            animateTimer.Stop();
            cpuTimer.Stop();
            notifyIcon.Visible = false;
            SafeCall(delegate () { settings?.SaveSettings(settingsFilename); });
            Application.Exit();
        }
    }
}
