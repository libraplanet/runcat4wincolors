# RunCat4WinColors
RunCat for windows Color Selectable Edition.
fork from https://github.com/Kyome22/RunCat_for_windows

## RunCat_for_windows
Original RunCat for windows is a cute running cat animation on your windows taskbar. Enjoy!


![Demo](images/runcat_demo.gif)

## 主な変更点
* システム デフォルトの猫の色を変更  
  * デフォルトは白猫 (ダーク テーマ用)  
    (シェルのExplorerがテーマに対応していない、Windows10 v1809以前への対応のため)
  * ライト テーマが有効のときのみ黒猫  
    ( ``SystemUsesLightTheme`` に ``1`` が設定されている時のみ)
* 色変更に対応  
  * 猫の色を任意の色に変更可能
  * 背景色を任意の色に設定可能
* 最後の色設定の記憶
  * 実行ファイルと同フォルダに設定ファイルを保存
* 実行プラット フォームを ``AnyCPU`` に変更

![ss](images/ss_00.png)
![ss](images/ss_01.png)
![ss](images/ss_02.png)

